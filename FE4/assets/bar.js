
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Libarary Activities', 'Percentage'],
          ["Stocks", 8],
          ["Books Supply", 4],
          ["Books Borrowed", 7],
          ['Damage Books', 1]
        ]);

        var options = {
          title: 'Books',
          width: 500,
          legend: { position: 'none' },
          chart: { title: 'Books',
                   subtitle: 'Library Activities' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'Percentage'} // Top x-axis.
            }
          },
          bar: { groupWidth: "50%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };
    