<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\Borrowed_bookscontroller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::resources([
    '/books' => BookController::class,
    '/returnedbooks' => Returend_bookscontroller::class,
    '/categories' => Categoriescontroller::class,
    '/borrowedbooks' => Borrowed_bookscontroller::class,
    '/patrons' => Patronscontroller::class,

]);