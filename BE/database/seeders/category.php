<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mul_rows= [
            [ 'category' => 'Adventure'],
            [ 'category' => 'Comedy'],
            [ 'category' => 'Novel'],
            [ 'category' => 'History'],
            [ 'category' => 'Scifi']
        ];
        
            foreach ($mul_rows as $rows) {
            Category::create($rows);
                }
            }
    }

